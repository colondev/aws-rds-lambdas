# AWS LAMDBAS

## Introduction
Collection of handy lambdas to use with AWS.

## Contents

### 1)ManualSnapshotLambda.py
AWS RDS Serverless backups can only be saved for 35 days. This lambda creates manual snapshots from a RDS aurora cluster, in order to provide an extra layer of backups.

#### Usage 
Use any way you like to trigger it, for example a Cloudwatch scheduled event.
    
