import boto3
import json
from datetime import datetime

"""
    This lambda creates a manual snapshot from the specified cluster
    Replace variables with your own values
"""
cluster_name = '' 
snapshot_suffix = 'manual-snapshot'

def lambda_handler(event, context):
    snapshot_name = prepare_new_snapshot_name()
    response = create_snapshot(snapshot_name) 
    return json.loads(json.dumps(response, default=str))

def prepare_new_snapshot_name():
    now = datetime.now()
    dateString = now.strftime("%Y-%m-%d") + '-'
    return cluster_name  + '-' + dateString + snapshot_suffix
 
def create_snapshot(snapshot_name):
    return boto3.client('rds').create_db_cluster_snapshot(
        DBClusterSnapshotIdentifier=snapshot_name,
        DBClusterIdentifier=cluster_name,
        Tags=[{ 'Key': snapshot_suffix, 'Value': 'made-by-lambda' }]
    )
